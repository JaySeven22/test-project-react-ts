import { stat } from 'fs';
import './style.scss'


interface ISearchPageItem {
  value: string;
}

const SearchPageItem: React.FC<ISearchPageItem> = ({value}) => {

  return (
    <ul className='searchPage'>
      <li className="searchPageItem">{value}</li>
    </ul>
  );
}


export default SearchPageItem;