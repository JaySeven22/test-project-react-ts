import React, { useState } from 'react';
import './style.scss'
import routeMain from "./route";
import {ReactComponent as Icon} from "../../assets/searchWhite.svg"
import axios from 'axios';
import SearchPageItem from './searchItem/searchItem';
import getAddress from '../../service/getAddress';




const SearchPage = () => {

  const [query, setQuery] = useState('');
  const [state, setState] = useState([]);
 

  const changeInput = (e: {target: {value: string}}) => {
    setQuery(e.target.value)
  }

  const onSubmit = async (e: { preventDefault: () => void; }) => {
    e.preventDefault()
    
    const result = getAddress(query); 
    
    setState((await result).data.suggestions)
   
    setQuery('')
  }
  
  return (
    <div className='searchPage'>
        <h1>Поиск адресов</h1>
        <p className='searchPageContent'>Введите интересующий вас адрес</p>
        <form onSubmit={onSubmit} className='searchPageValue'>
            <input onChange={changeInput} value={query} className='searchPageValueInput' placeholder='Введите интересющий вас адрес'/>
            <button type='submit' className='searchPageValueBtn'>
            <Icon/> 
                Поиск
            </button>
        </form>
        <div className="searchPageContentItem">
          <div className={(state.length > 0? "searchPageItem" : "")}>
            <h2 className='searchPageItemTitle'>{state && state.length > 0 ? 'Адреса' : null}</h2>
            {state ? state.map((item: { value: string; data: { kladr_id: React.Key | null | undefined; }; }) => <SearchPageItem value={item.value}  key={item.data.kladr_id}/> ) : null}
          </div>
        </div>
    </div>
  );
}

export {routeMain}

export default SearchPage;
