import React from 'react';
import './style.scss'
import routeMain from "./route";
import Mock from "../../service/mock.json"
import parse from 'html-react-parser';


const MainPage = () => {
  
  
  return (
    <div className='mainPage'>
        <h1>Новости</h1>
        <h2>{Mock.news[0].title}</h2>
        <div className='mainPageContent'>
          {parse(Mock.news[0].body)}
        </div>
        
    </div>
  );
}

export {routeMain}

export default MainPage;
