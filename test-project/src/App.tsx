import { BrowserRouter } from 'react-router-dom'
import AppContent from './components/AppContent/AppContent';
import './App.css';


const App = () => {
  return (
    <BrowserRouter>
      <AppContent />
    </BrowserRouter>
  );
}

export default App;
