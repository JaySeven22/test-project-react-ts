import React from 'react';
import './style.scss'
import Ellipse from "../../assets/ellipse.svg"
import Vector from "../../assets/Vector.svg"


const Header = () => {
  return (
    <header className="Header">
        <nav className='headerWrapper'>
            <div className='headerWrapperContent'>
                <p className='contentBlock'>
                    <img alt='logo' className='image' src={Vector}/>
                    <h2>Wrench CRM</h2>
                </p>
                <p className='contentBlock'>
                <img alt='logo' className='image' src={Ellipse}/>
                    <h2>Имя Фамилия</h2>
                </p>
            </div>
        </nav>
    </header>
  );
}

export default Header;
