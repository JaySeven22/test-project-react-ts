import React, { useState } from 'react';
import './style.scss'
import NavigationItem from '../NavigationItem/NavugationItem';
import Dom from "../../assets/dom.svg"
import kalendar from "../../assets/kalendar.svg"
import Search from "../../assets/search.svg"
import Kart from "../../assets/Kart.svg"
import table from "../../assets/table.svg"
import vijet from "../../assets/vijet.svg"
import settings from "../../assets/settings.svg"
import group from "../../assets/Group.svg"
import out from "../../assets/out.svg"
import finance from "../../assets/finance.svg"
import {routeMain as routeMainPage} from '../../pages/MainPage/MainPage';
import {routeMain as routeSearchPage} from '../../pages/searchPage/SearchPage';
import Burger from '../BurgerMenu/BurgerMenu';


function Navigation() {


  const [state, setState] = useState(true);


  const childView = () => {
    setState(!state)
  }
  
  return (
    <Burger>
    <div className='navigation'>
        <h2>Меню</h2>
        <ul className='navigationContent'>
            <NavigationItem title={'Главная'} route={routeMainPage} image={Dom}/>
            <NavigationItem title={'Поиск адресов'} route={routeSearchPage} image={Search}/>
            <NavigationItem title={'Таблицы'} image={table} />
            <NavigationItem title={'Календарь'} image={kalendar}  />
            <NavigationItem title={'Карты'} image={Kart}/>
            <NavigationItem title={'Виджеты'} image={vijet}/>        
            <NavigationItem state={state} childrenView={childView} title='Настройки' image={settings} >
              <NavigationItem title='Настройки профиля' image={group}/>
              <NavigationItem title='Управление финансами' image={finance}/>
              </NavigationItem>
            <NavigationItem title={'Выход'} image={out}/>
        </ul> 
    </div>
    </Burger>
  );
}

export default Navigation;
