import React from 'react';
import Header from '../Header/Header';
import Navigation from '../Navigation/Navigation';
import './style.scss'

import {Route, Switch, Redirect} from 'react-router-dom';

import MainPage, {routeMain as routeMainPage} from '../../pages/MainPage/MainPage';
import SearchPage, {routeMain as routeSearchPage} from '../../pages/searchPage/SearchPage';


const AppContent = () => {

  return (
    <div className="App">
      <Header />
      <main>
      <Navigation/>
        <Switch>
          <Route exact path={routeMainPage()} component={MainPage} />
          <Route exact path={routeSearchPage()} component={SearchPage} />
          <Redirect
            to= {{
              pathname: routeMainPage()
            }}
            />
        </Switch>
      </main>
    </div>
  );
}

export default AppContent;
