import React from "react"
import "./style.scss"
import Hamburger from "../../assets/hamburger.svg"

interface IBurger {
  children: React.ReactNode
}

const Burger: React.FC<IBurger> = ({children}) => {

  const [isExpanded, setIsExpanded] = React.useState(true);
  
  return (
    <>
    <button onClick={() => setIsExpanded(!isExpanded)}><img src={Hamburger} /></button>
    <div 
      className={(isExpanded ? "burgerOpen" : "burger")}
    >
      {children}
    </div>
    </>
  )
}



export default Burger;