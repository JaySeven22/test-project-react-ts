import React from 'react';
import { Link } from 'react-router-dom';
import './style.scss'


interface INavigationItem {
  title: string,
  image?: string,
  children?: React.ReactNode[],
  setting?: string,
  childrenView?: () => void ,
  route?: () => string,
  state?: boolean
}

const NavigationItem: React.FC<INavigationItem> = ({title, image, children, setting, childrenView, route, state}) => {
  
  return (
    <div onClick={childrenView} className='navigation'>
      {route? <Link style={{textDecoration: 'none'}} to={route()? route : ''}>
      <ul className='navigationContent'>
            <li className='navigationContentItem'>
                <img  src={image} />
                {title}
                <img className='navigationContentItemSettings' src={setting} />
            </li>
            <div className='navigationContentMap'>
            <p className={state == false ? "navigationContentMapText" : "navigationContentMapTextOpen"  }>{children ? children : null} </p>
            </div>
        </ul> 
      </Link> : <div>
      <ul className='navigationContent'>
            <li className='navigationContentItem'>
                <img  src={image} />
                {title}
                <img className='navigationContentItemSettings' src={setting} />
            </li>
            <div className='navigationContentMap'>
            <p className={state == false ? "navigationContentMapText" : "navigationContentMapTextOpen"  }>{children ? children : null} </p>
            </div>
        </ul> 
        </div>}
    </div>
  );
}

export default NavigationItem;
