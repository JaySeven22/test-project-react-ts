import axios from "axios";


const getAddress = (query : string) => {

    var token = "0d0107c68fe9f7a671a3e2e63448ab81b35c3350"

    const response =  axios.post(
        "https://suggestions.dadata.ru/suggestions/api/4_1/rs/suggest/address",
      {
        query
      },
      {
        headers: {
        "Content-Type": "application/json",
        "Accept": "application/json",
        "Authorization": "Token " + token
        },
      }
    )
    
    return response
}

export default getAddress ;